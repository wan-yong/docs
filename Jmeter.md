## WebDriver来完成GUI的功能测试 https://jmeter-plugins.org/wiki/WebDriverSampler/

# 前提：
安装 https://jmeter-plugins.org/wiki/PluginsManager/
# 功能测试配置说明 https://blog.csdn.net/meitingbee/article/details/53924469

## jmeter和loadrunner测试结果差异大-web页面静态资源下载 http://www.voidcn.com/article/p-bqozpeqt-bph.html

描述： 一个简单的web页面，同样50并发用户，响应时间十几毫秒，Loadrunner测试结果TPS=1500，jmeter测试结果TPS=4000+

web页面静态资源下载 

1. Loadrunner默认配置的是下载静态资源，每次迭代模拟新用户，清除缓存，所以为了更好的模拟用户的使用习惯，通常是不下载静态资源，即将上面三个选项取消勾选
2. Jmeter默认配置的是不下载静态资源，这点与Loadrunner正好相反

web页面中通常包含很多静态资源（非HTML资源），如js、css、图片等，这些资源可能不会经常变化，如果每次都去服务器端加载这些资源，会浪费时间和带宽，所以浏览器会把这列不常用的静态资源缓存，在使用工具压测的时候，是否模拟浏览器缓存，是否每次下载静态资源会对性能有非常大的影响。这也是本次导致jmeter和Loadrunner测试结果差距很大的原因。

## Dytrace收集结果 https://www.oschina.net/translate/analyzing-jmeter-application-performance-results?print

## LR 和 Jmeter比较 https://testerhome.com/topics/8871


