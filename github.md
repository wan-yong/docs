## git 用远程覆盖本地
### 正确的做法应该
```
git fetch --all # 下载远程的库的内容，不做任何的合并
git reset --hard origin/master # 把HEAD指向刚刚下载的最新的版本
```

##  使用Github协同开发

Todo:
1.  [打版本标签，例如：v0.9 v1.0 v1.4](https://www.liaoxuefeng.com/wiki/896043488029600/900788941487552)

git标签分为2种：轻量标签、附注标签

轻量标签 - 指向提交对象的引用；附注标签 - 仓库中的一个独立对象

建议使用附注标签，加 -a 参数

~~~
# 打标签
git tag -a v0.9 -m “0.9版本-暂不支持influxDB”

# 提交标签
git push origin v0.9

# 查看标签
git tag
~~~
2.  [遵循github flow，创建分支](https://www.ibm.com/developerworks/cn/java/j-lo-git-mange/index.html#githubflow)

*  使用fork创建自己账号名下的分支，并clone到本地电脑做开发。
`git clone https://github.com/child/children.git` 

~~~
┌─ GitHub ──────────────────────────────────────┐
│                                               │
│ ┌─────────────────┐       ┌─────────────────┐ │
│ │ twbs/bootstrap  │─fork─>│  my/bootstrap   │ │
│ └─────────────────┘       └─────────────────┘ │
│                                  ▲            │
└──────────────────────────────────┼────────────┘
                                   ▼
                          ┌─────────────────┐
                          │ local/bootstrap │
                          └─────────────────┘
~~~

*  添加父级远程repo。
`git remote add upstream https://github.com/parent/parent.git`
*  查看本地repo对应的父级、子级远程repo。
`git remote --verbose`
*  添加本地的修改（目录结构、文件）到“stage”。
`git add .`
*  查看提交历史
`git log`
*  查看命令历史
`git reflog`
*  返回指定的commit（例如，1094a）
`$ git reset --hard 1094a`
*  提交修改到本地repo。
`git commit -m "message xxx."`
*  同步父级远程repo。
`git fetch upstream`
*  合并本地master分支和upsteram/master分支,将原项目中的更改更新到本地分支，这样就能使你的本地的fork分支与原项目保持同步
`git merge master upstream/master`
*  推送到子级远程repo（即fokr分支的repo）。
`git push`
*  登陆github网页，从子级repo提交pull request。
