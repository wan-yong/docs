# 上手

k8s半个月起步，三个月手熟。先学一堆新概念，然后才能动手折腾，没人指点少不了各种磕磕碰碰。

# 硬件

要求高，master要1台机器专门跑，master高可用要3台机器

# 功能颗粒度

更细

# 社区活跃

活跃高

# k8s与区块链
https://containerjournal.com/2017/03/22/ibm-couples-docker-containers-blockchain-database/
https://opensource.com/article/18/4/deploying-hyperledger-fabric-kubernetes
https://www.xenonstack.com/blog/devops/blockchain-deployment-microservices-docker/