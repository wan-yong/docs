# k8s集群示意图

![k8s cluster](https://d33wubrfki0l68.cloudfront.net/99d9808dcbf2880a996ed50d308a186b5900cec9/40b94/docs/tutorials/kubernetes-basics/public/images/module_01_cluster.svg)

# 基本概念
http://dockone.io/article/932

# kubectl部署

## 第一个终端：
    kubectl get nodes //取node信息
    kubectl get deployments //取部署信息
    kubectl run kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1 --port=8080 //指定deployment name, --port and app image location (full repo url for images hosted outside Docker hub)
    export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}') //设置Pod参数
    curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/ //向Pod里的程序，发送http请求

## 第二个终端：
    kubectl proxy //启动代理


# kubectl使用Pod变量，进入Pod容器
    kubectl exec $POD_NAME env // 列出环境变量
    kubectl exec -it $POD_NAME bash // 进入Pod容器
## 例子
    [root@dev-master hzb]# kubectl exec -it hzb-mongo1-ceph bash
    root@hzb-mongo1-ceph:/# tail -f a.log
    root@hzb-mongo1-ceph:/# ctrl+c