1. 链接
```
[淘宝网](http://www.taobao.com/)
```

2. 图片
```
![图片标题](http://shuju.taobao.ali.com/images/web/tsj_main_dirc_img.png)
```

3. 标题 - 几个 # 就是几级标题，最小到六级
```
# 一级标题
## 二级标题
### 三级标题
#### 四级标题
##### 五级标题
###### 六级标题
```

4. 斜体
```
*这是斜体*
*[这是斜体链接](http://www.taobao.com)*
```

5. 颜色
```
 这是<label style="color:red">红色</label>字体
 这是<label style="color:green">绿色</label>字体
 这是<label style="color:yellow">黄色</label>字体
 这是<label style="color:blue">蓝色</label>字体
 <label style="color:red">tips:修改color为对应的颜色英文字母即可，复杂的颜色不要想了，况且大家也用不到</label>
```

6. 加粗
```
 **加粗**字体
```

7. 有序、无序列
```
 * list1
 * list2
 * list3

 1. list1
 2. list2
 3. list3
```

8. 分割线
```
***
```

9. Code
```
```

10. 内容框、换行、中划线、脚注、表格，不常用，未列出。