前端：
获取前端表单传来的数据
1. get的传值被放入了req.query对象；
2. post的传值被放入req.body对象；
3. 使用req.params对象，可以取到两种方式的值

ajaxSubmit的使用，禁止表单跳转
https://blog.csdn.net/GATSBYER/article/details/72593827

必须给表单的字段设置name属性，才能使用ajaxForm()方法提交

纯ajax的提交，没有使用jquery.form插件
https://blog.csdn.net/u011458469/article/details/48134581

后端：
参考(包括session验证)
https://www.cnblogs.com/zhongweiv/p/nodejs_express_webapp1.html
https://www.jb51.net/article/127876.htm
https://www.zhangshengrong.com/p/3mNmyryNj0/ jsp的用法
http://www.cnblogs.com/liuhongfeng/p/5149301.html jsp的用法

Jquery官网
http://jquery.malsup.com/form/
http://www.h-ui.net/lib/jQuery.form.js.shtml 中文

前后端交互演示 python
https://www.jianshu.com/p/4350065bdffe
传值被放入request.form.id对象；

