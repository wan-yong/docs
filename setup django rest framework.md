# 生产环境，无网络机器执行安装配置
## 创建新用户，做开发账号
创建
```
[robot@rhel75a ~]$ adduser robot
[robot@rhel75a ~]$ passwd robot
```
查看
```
[robot@rhel75a ~]$ id robot
uid=1000(robot) gid=1000(robot) groups=1000(robot)
[robot@rhel75a ~]$ groups robot
robot : robot
```
创建rpm和pip目录，作为rpm和pip的下载源
```
[robot@rhel75d ~]$ mkdir -p /home/robot/rpm
[robot@rhel75d ~]$ mkdir -p /home/robot/pip
```

## 使用联网机器下载python3.6的rpm包
```
yum install --downloadonly --downloaddir=/home/robot/rpm rh-python36-numpy rh-python36-scipy rh-python36-python-tools rh-python36-python-six
```
安装依赖包
```
yum -y install gcc
yum -y install libgfortran libquadmath scl-utils tcl tk atlas
yum -y install scl-utils-build-20130529-19.el7.x86_64.rpm
```

## 切换到root用户，卸载默认的postgresql，安装9.6版本的postgresql

### 卸载默认的postgre
```
rpm -aq | grep postgres #查看是否安装了postgres
yum -y remove postgresql* #删除旧版本
```
### 追加下条记录到Red Hat的/etc/yum/pluginconf.d/rhnplugin.conf [main] 区域
编辑文件
```
vi /etc/yum/pluginconf.d/rhnplugin.conf
```
修改如下
```
exclude=postgresql*
```
### 安装RPM
```
yum install --downloadonly --downloaddir=/home/robot/rpm https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7.5-x86_64/pgdg-redhat96-9.6-3.noarch.rpm

或者reinstall再次下载
yum reinstall --downloadonly --downloaddir=/home/robot/rpm https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7.5-x86_64/pgdg-redhat96-9.6-3.noarch.rpm
```
如果碰到错误(can not find RHNS CA file)，可以通过如下方式解决：
```
# cp /usr/share/rhn/RHNS-CA-CERT /usr/share/rhn/RHN-ORG-TRUSTED-SSL-CERT
# yum clean all
```

### 安装5个依赖包
```
yum install --downloadonly --downloaddir=/home/robot/rpm postgresql96.x86_64 postgresql96-server.x86_64 postgresql96-devel.x86_64 postgresql96-libs.x86_64 postgresql96-contrib.x86_64

或者，使用reinstall再次下载，将Postgres安装包放置在同一个文件夹中， 执行安装命令

rpm -ivh postgresql96-*.rpm # rpm自动解决依赖关系
```

### 修改/etc/profile文件， 将Postgres命令加入系统路径
```
vi /etc/profile
# 在文件最后加入以下语句
export PATH=$PATH:/usr/pgsql-9.6/bin
# 重新加载配置文件
source /etc/profile
```

### 进入pgsql，初始化数据库，配置开机启动
```
[root@rhel75c ~]# cd /usr/pgsql-9.6/bin/
[root@rhel75c bin]# /usr/pgsql-9.6/bin/postgresql96-setup initdb
Initializing database ... OK

[root@rhel75c bin]# systemctl enable postgresql-9.6
Created symlink from /etc/systemd/system/multi-user.target.wants/postgresql-9.6.service to /usr/lib/systemd/system/postgresql-9.6.service.
[root@rhel75c bin]# systemctl start postgresql-9.6

# 切换到postgres数据库
[root@rhel75c bin]# su - postgres

# 使用数据库自带的postgres用户登录数据库,并为其赋予密码
-bash-4.2$ psql -U postgres
```

### 创建DB的账号
```
CREATE USER djangousr PASSWORD 'passw0rd';
CREATE DATABASE djangodb OWNER djangousr;
```

### 修改postgres登陆配置文件
```
vi /var/lib/pgsql/9.6/data/pg_hba.conf

# 修改DB密码认证方式，从ident改为md5，对密码进行加密。

# IPv4 local connections:
host    all             all             127.0.0.1/32            ident #change to md5
# IPv6 local connections:
host    all             all             ::1/128                 ident #change to md5
```

### 修改之后，重启DB
```
systemctl restart postgresql-9.6
```

## 安装venv虚拟环境，进行多版本python的环境隔离
创建目录，进入该目录，安装隔离环境
```
[robot@rhel75a ~]$ scl enable rh-python36 bash
[robot@rhel75a ~]$ mkdir ~/rtgpfs
[robot@rhel75a ~]$ cd ~/rtgpfs
[robot@rhel75a rtgpfs]$ python3 -m venv py36-venv
[robot@rhel75a rtgpfs]$ source py36-venv/bin/activate
```
检查结果，出现(py36-venv)表示已经进入虚拟环境
```
(py36-venv) [robot@rhel75a rtgpfs]$ python3 -V
Python 3.6.3
(py36-venv) [robot@rhel75a rtgpfs]$ which python
~/rtgpfs/py36-venv/bin/python
(py36-venv) [robot@rhel75a rtgpfs]$ scl -l
rh-python36
```
### ******** 很重要 备注： 重新使用robot用户登陆，或者新开session窗口，执行如下
```
[root@rhel75a ~]$ su - robot # 可选，如果用root用户登陆，需要切换到robot用户
[robot@rhel75a ~]$ scl enable rh-python36 bash
[robot@rhel75a rtgpfs]$ source ~/rtgpfs/py36-venv/bin/activate
```

## 在可以联网的开发机器上，升级pip到最新版本，并且，安装Django、REST框架 
升级pip到最新版本
```
(py36-venv) [robot@rhel75a rtgpfs]$ pip install --upgrade pip
```
安装Django、REST框架 
```
(py36-venv) [robot@rhel75a rtgpfs]$ pip install "Django~=2.2,<2.3"
(py36-venv) [robot@rhel75a rtgpfs]$ pip install djangorestframework
```

## 导出已安装的pip包列表。并且，指定安装源，比如国内通过豆瓣可能比较快，下载pip包
查看已安装的pip包列表
```
(py36-venv) [robot@rhel75d ~]$ pip freeze >requirements.txt
(py36-venv) [robot@rhel75d ~]$ ls ./requirements.txt
./requirements.txt
(py36-venv) [robot@rhel75d ~]$ cat ./requirements.txt
Django==2.2.3
djangorestframework==3.9.4
psycopg2==2.8.3
pytz==2019.1
sqlparse==0.3.0
```
下载离线安装包
```
(py36-venv) [robot@rhel75a rtgpfs]$ pip download -d /home/robot/pip -r requirements.txt -i https://pypi.douban.com/simple
```

## 将pip文件夹和requirement.txt拷贝至离线机器，比如，/home/robot/pip下，requirements.txt文件放在pip运行目录，比如，/home/robot/rtgpfs/py36-venv/bin/pip
```
# 加载环境变量
(py36-venv) [robot@rhel75a ~]$ source /etc/profile
# 安装python包
(py36-venv) [robot@rhel75a ~]$ pip install --no-index --find-links=/home/robot/pip -r requirements.txt
```

## 创建项目文件夹
```
(py36-venv) [robot@rhel75a dev]$ mkdir ~/dev
(py36-venv) [robot@rhel75a dev]$ cd ~/dev
(py36-venv) [robot@rhel75a dev]$ python -m django --version
2.2.3
(py36-venv) [robot@rhel75a dev]$ django-admin startproject gpfs
```

## 创建app文件夹
```
(py36-venv) [robot@rhel75a dev]$ cd gpfs
(py36-venv) [robot@rhel75a gpfs]$ django-admin startapp auto
```

## 修改配置文件settings.py允许任何host访问
```
(py36-venv) [robot@rhel75a gpfs]$ vi ./gpfs/settings.py

# 修改ALLOWED HOST = ['*',]; #保留,号
ALLOWED HOST = ['*',];
```
## 继续修改settings.py配置数据库
```
(py36-venv) [robot@rhel75a gpfs]$ vi ./gpfs/settings.py

# 注释掉原有的配置，修改成自己的数据库参数
# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

#DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#    }
#}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'djangodb',
        'USER': 'djangousr',
        'PASSWORD': 'passw0rd',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
```

## 关闭防火墙
```
[root@rhel75c ~]# systemctl stop firewalld
[root@rhel75c ~]# systemctl disable firewalld
```

## 启动服务
```
# 首次启动前，需要同步数据库。之后，每次更新数据库，也需要同步。
python manage.py migrate

# 启动服务
(py36-venv) [robot@rhel75a gpfs]$ python3 manage.py runserver 0.0.0.0:8000
```

## 浏览器查看
敲入网址和8000端口
![查看](/pic/success.png)


## 附录: 下载python3.6.3安装包
下载、解压
```
wget https://www.python.org/ftp/python/3.6.3/Python-3.6.3.tar.xz
xz -d Python-3.6.7.tar.xz
tar -xf Python-3.6.7.tar
```
解决编译安装python3所需的软件依赖   ******** 很重要
```
yum install --downloadonly --downloaddir=/root/rpm gcc patch libffi-devel python-devel  zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel
```
编译安装
```
./configure --prefix=/opt/python36/
make
make install
```
将此配置写到profile最低行
```
 vim  /etc/profile
```
添加到PATH
```
　　PATH=/opt/python36/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin
```
读取这个文件，让python3生效
```
　　source /etc/profile
```

## 生产环境
### 修改settings.py关闭Debug
```
DEBUG = false
```