#### Advanced Shell

1. simple arguments and getopts - make it a real Shell command. 
2. looping and functions for performing complex operations. 
3. Regex and regular expressions - validate the arguments. 
4. logs and I/O redirections

For sample code, visit http://www.PacktPub.com/support

#### API - Web Framework
1. 将Python脚本包装在Web框架中，它们称为应用程序接口（API）

2. Falcon是一个轻量级，易于部署的  Web 服务器网关接口 （WSGI）。与Gunicorn（另一个轻量级服务器）一起，它充当可扩展的解决方案，在资源使用方面非常轻松。资源使用成为Web框架的一个非常关键的组件，因为它可以扩展以处理数千个API请求。 

```
# Falcon, Gunicorn安装命令如下：
pip install falcon
pip install gunicorn
```
