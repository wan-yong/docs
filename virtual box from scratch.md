# 选择x64

# 双网卡设置
* 坑 - 关掉cisco VPN，或者其他VPN。它们阻止上网。网卡配置文件在

# 红帽开发者rpm源设置
## 注册账号以后，用命令行登陆。并且，激活对应的repo仓库。
```
[root@rhel75a ~]# subscription-manager register --username wan_yong --password zse45rdxM5 --auto-attach
[root@rhel75a ~]# subscription-manager repos --enable rhel-7-server-optional-rpms --enable rhel-server-rhscl-7-rpms

[root@rhel75a ~]# subscription-manager repos --disable rhel-7-server-optional-rpms --disable rhel-server-rhscl-7-rpms
[root@rhel75a ~]# subscription-manager unregister
```
# 本机挂载iso镜像源设置
## /dev/sr0 作为本地dvd，如果有多个dvd，依次为sr0, sr1, ..., srn 
```
[root@rhel75d ~]# mount /dev/sr0 /mnt/cdrom
mount: /dev/sr0 is write-protected, mounting read-only
```
备注：需要在virtualbox虚机里面，对应添加dvd0到dvdn

## 检查
```
[root@rhel75d ~]# df -h
Filesystem             Size  Used Avail Use% Mounted on
/dev/mapper/rhel-root   17G  8.6G  8.5G  51% /
devtmpfs               908M     0  908M   0% /dev
tmpfs                  920M  4.0K  920M   1% /dev/shm
tmpfs                  920M  8.6M  911M   1% /run
tmpfs                  920M     0  920M   0% /sys/fs/cgroup
/dev/sda1             1014M  142M  873M  14% /boot
tmpfs                  184M     0  184M   0% /run/user/0
/dev/sr0               4.4G  4.4G     0 100% /mnt/cdrom
```

# 红帽rhel7.5网卡设置
## 文件位置
```
/etc/sysconfig/network-scripts/
```
例如，每个ifcfg文件代表1张网卡：
```
/etc/sysconfig/network-scripts/ifcfg-enp0s3
/etc/sysconfig/network-scripts/ifcfg-enp0s8
/etc/sysconfig/network-scripts/ifcfg-lo
```

## 说明
* 配置文件大小写严格区分(不能更改配置内字符的大小写)
* 如果打开了dhcp则静态配置会失效
* 配置文件各项没有顺序区分

## 主要配置有（待补充）
```
DEVICE  ---->设备名称
BOOTPROTO  --->ip分配方式  dhcp，static ，none 或者把该行删除
IPADDR        ---ip地址
GATEWAY       ---网关
DNS1          ---dns配置
ONBOOT=yes      --- 是否开机启动该网卡设备
```
## 常用命令
* 查看ip ip addr
* 配置网卡命令  ifconfig
* 查看网络是否配置好  route -n
* 该文件从 /etc/resov.conf  读取信息。信息从程序生成，更改无效
* 重启网卡 service network restart

# Linux
## 二进制安装go、nodejs
### 架构 - 只选择x64的二进制包
x86_64,x64,AMD64基本上是同一个东西。x86、x86_64主要的区别就是32位和64位的问题，
### 配置文件
* 多用户 /etc/profile
* 单用户 ~/.profile
### 目录 <br> /usr/local/
### 软链接
```
# node js 从安装文件夹链接到/usr/bin/
sudo ln -s /usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/node /usr/bin/node

sudo ln -s /usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/npm /usr/bin/npm

sudo ln -s /usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin/npx /usr/bin/npx
```